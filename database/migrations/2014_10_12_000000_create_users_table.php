<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('username');
            $table->string('employed');
            $table->string('country');
            $table->string('city');
            $table->string('zip');
            $table->string('citation');
            $table->string('academic_discipline');
            $table->string('language');
            $table->string('degree');
            $table->string('detailed_cv');
            $table->string('brief_cv');
            $table->string('cert_title');
            $table->string('cert_file_one');
            $table->string('id_title');
            $table->string('id_image_one');
            $table->string('title_sample_one');
            $table->string('sample_one_file');
            $table->string('title_sample_two');
            $table->string('sample_two_file');
            $table->string('payment_method');
            $table->string('payment_details');
            $table->string('mobile_number');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
