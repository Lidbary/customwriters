<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role;
        $role_admin->name = 'admin';
        $role_admin->description = 'the admin';
        $role_admin->save();

        $role_editor = new Role;
        $role_editor->name = 'editor';
        $role_editor->description = 'the editor';
        $role_editor->save();


        $role_writer = new Role;
        $role_writer->name = 'writer';
        $role_writer->description = 'a writer';
        $role_writer->save();
    }
}
