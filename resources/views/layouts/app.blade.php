<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Ewakili</title>
    

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
     {!! Html::style('client/bower_components/uikit/css/uikit.almost-flat.min.css') !!}
     {!! Html::style('client/assets/css/login_page.min.css') !!}

  

</head>

     @yield('content')

    {!! Html::script('client/assets/js/common.min.js') !!}
    {!! Html::script('client/assets/js/uikit_custom.min.js') !!}
    {!! Html::script('client/assets/js/altair_admin_common.min.js') !!}
    {!! Html::script('client/assets/js/pages/login.min.js') !!}
      
  </body>
</html>