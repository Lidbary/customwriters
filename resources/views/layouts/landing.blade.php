<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
    {!! Html::style('landing/assets/fonts/profession/style.css') !!}
    {!! Html::style('landing/assets/libraries/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('landing/assets/libraries/bootstrap-fileinput/css/fileinput.min.css') !!}
    {!! Html::style('landing/assets/libraries/bootstrap-select/css/bootstrap-select.min.css"') !!}
    {!! Html::style('landing/assets/libraries/bootstrap-wysiwyg/bootstrap-wysiwyg.min.css') !!}
    {!! Html::style('landing/assets/css/profession-blue-navy.css') !!}

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">
    
    <title>Thecustomwriters</title>
</head>


<body class="hero-content-dark footer-dark">

<div class="page-wrapper">
    <div class="header-wrapper">
    <div class="header">
        <div class="header-top">
            <div class="container">
                <div class="header-brand">
                    <div class="header-logo">
                        <a href="{{url('/')}}">
                            <i class="profession profession-logo"></i>
                            <span class="header-logo-text">Thecustom<span class="header-logo-highlight">.</span>Writers</span>
                        </a>
                    </div><!-- /.header-logo-->

                    <div class="header-slogan">
                        <span class="header-slogan-slash">/</span>
                        <span class="header-slogan-text">Leading Writing Agency</span>
                    </div><!-- /.header-slogan-->
                </div><!-- /.header-brand -->

                <ul class="header-actions nav nav-pills">
                @if (Auth::guest())
                    <li><a href="{{url('register')}}" class="primary">Create Account</a></li>
                    @else
                    <li><a href="{{url('home')}}" class="primary">My Work Desk..</a></li>
                    <li><a href="{{url('logout')}}" class="primary">Logout</a></li>
                    @endif
                </ul><!-- /.header-actions -->

                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.container -->
        </div><!-- /.header-top -->

    </div><!-- /.header -->
</div><!-- /.header-wrapper-->


         @yield('content')


    

    <div class="footer-wrapper">
    <div class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="footer-top-block">
                            <h2><i class="profession profession-logo"></i> Thecustom Writers</h2>

                            <p>
                                Fusce congue, risus et pulvinar cursus, orci arcu tristique lectus, sit amet placerat justo ipsum eu diam. Pellentesque tortor urna, pellentesque nec molestie eget, volutpat in arcu. Maecenas a lectus mollis.
                            </p>

                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                            </ul>
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="footer-top-block">
                            <h2>Helpful Links</h2>

                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Support</a></li>
                                <li><a href="#">License</a></li>
                                <li><a href="#">Affiliate</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="#">Terms &amp; Conditions</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3">
                        <div class="footer-top-block">
                            <h2>Trending Jobs</h2>

                            <ul>
                                <li><a href="position-detail.html">Android Developer</a></li>
                                <li><a href="position-detail.html">Senior Teamleader</a></li>
                                <li><a href="position-detail.html">iOS Developer</a></li>
                                <li><a href="position-detail.html">Junior Tester</a></li>
                                <li><a href="position-detail.html">Full Stack Developer</a></li>
                                <li><a href="position-detail.html">Node.js Developer</a></li>
                                <li><a href="position-detail.html">Scala Developer</a></li>
                            </ul>
                        </div><!-- /.footer-top-left -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-top -->

        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-left">
                    &copy;  2016 All rights reserved.
                </div><!-- /.footer-bottom-left -->

                <div class="footer-bottom-right">
                    Created by <a href="">Thecustomwriters</a>
                </div><!-- /.footer-bottom-right -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom -->
    </div><!-- /.footer -->
</div><!-- /.footer-wrapper -->

</div><!-- /.page-wrapper -->


{!! Html::script('landing/assets/js/jquery.js') !!}
{!! Html::script('landing/assets/js/jquery.ezmark.js') !!}
{!! Html::script('landing/assets/libraries/bootstrap-sass/javascripts/bootstrap/collapse.js') !!}
{!! Html::script('landing/assets/libraries/bootstrap-sass/javascripts/bootstrap/dropdown.j') !!}
{!! Html::script('landing/assets/libraries/bootstrap-sass/javascripts/bootstrap/tab.js') !!}
{!! Html::script('landing/assets/libraries/bootstrap-sass/javascripts/bootstrap/transition.js') !!}
{!! Html::script('landing/assets/libraries/bootstrap-fileinput/js/fileinput.min.js') !!}
{!! Html::script('landing/assets/libraries/bootstrap-select/js/bootstrap-select.min.js') !!}
{!! Html::script('landing/assets/libraries/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js') !!}
{!! Html::script('landing/assets/libraries/cycle2/jquery.cycle2.min.js') !!}
{!! Html::script('landing/assets/libraries/cycle2/jquery.cycle2.carousel.min.js') !!}
{!! Html::script('landing/assets/libraries/countup/countup.min.js') !!}
{!! Html::script('landing/assets/js/profession.js') !!}


</body>
</html>