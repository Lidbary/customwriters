@extends('layouts.landing')

@section('content')
         <div class="main-wrapper">
    <div class="main">

    <div class="hero-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <h1>Write Quality. Get Paid Competitively</h1>
                <h2>Home of Freelance Writers! We offer competitive industry rates for our writers! We are currently hiring, check out our registration page and sign up today!.</h2>

                <a href="create-resume.html" class="hero-content-action">Sign Up Now</a>
            </div><!-- /.col-* -->

            <div class="col-sm-6 col-md-5 col-md-offset-1">
                <div class="hero-content-carousel">
                    <h2 style="text-align:center">Please sign in</h2>

                 
          
             <form role="form" method="POST" action="{{ url('/login') }}">
             {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="form-login-username">Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div><!-- /.form-group -->

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="form-login-password">Password</label>
                    <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div><!-- /.form-group -->

                <div class="checkbox">
                    <label><input type="checkbox" name="remember"> Keep me signed in</label>

                    <a href="{{url('/password/reset')}}" class="link-not-important pull-right">Forgot Password</a>
                </div><!-- /.checkbox -->

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                </div><!-- /.form-group -->

                <hr>

            
            </form>
    
   
                    
                </div><!-- /.hero-content-content -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.hero-content -->


<div class="stats">
    <div class="container">
        <div class="row">
            <div class="stat-item col-sm-4">
                <strong id="stat-item-1">20000</strong>
                <span>Orders Completed</span>
            </div><!-- /.col-* -->

            <div class="stat-item col-sm-4">
                <strong id="stat-item-2">600</strong>
                <span>Writers</span>
            </div><!-- /.col-* -->

            <div class="stat-item col-sm-4">
                <strong id="stat-item-3">ksh. 300</strong>
                <span>CPP</span>
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.stats -->


<div class="container">
    <div class="filter">
    <h2>Top Orders by Discipline</h2>
    <form method="get" action="">
      

        <ul class="filter-list">
            <li><a href="positions.html">Alabama <span class="filter-list-count">(1 704)</span></a></li>
            <li><a href="positions.html">Alaska <span class="filter-list-count">(1 215)</span></a></li>
            <li><a href="positions.html">Arizona <span class="filter-list-count">(904)</span></a></li>
            <li><a href="positions.html">Arkansas <span class="filter-list-count">(2 744)</span></a></li>
            <li><a href="positions.html">California <span class="filter-list-count">(904)</span></a></li>
            <li><a href="positions.html">Colorado <span class="filter-list-count">(1 804)</span></a></li>
            <li><a href="positions.html">Connecticut <span class="filter-list-count">(875)</span></a></li>
            <li><a href="positions.html">Delaware <span class="filter-list-count">(546)</span></a></li>
            <li><a href="positions.html">Florida <span class="filter-list-count">(7 895)</span></a></li>
            <li><a href="positions.html">Georgia <span class="filter-list-count">(4 562)</span></a></li>
            <li><a href="positions.html">Hawaii <span class="filter-list-count">(1 658)</span></a></li>
            <li><a href="positions.html">Idaho <span class="filter-list-count">(1 354)</span></a></li>
            <li><a href="positions.html">Illinois <span class="filter-list-count">(6 988)</span></a></li>
            <li><a href="positions.html">Indiana <span class="filter-list-count">(1 235)</span></a></li>
            <li><a href="positions.html">Iowa <span class="filter-list-count">(4 563)</span></a></li>
            <li><a href="positions.html">Kansas <span class="filter-list-count">(1 549)</span></a></li>

            <li><a href="positions.html">Kentucky <span class="filter-list-count">(432)</span></a></li>
            <li><a href="positions.html">Louisiana <span class="filter-list-count">(4 123)</span></a></li>
            <li><a href="positions.html">Maine <span class="filter-list-count">(923)</span></a></li>
            <li><a href="positions.html">Maryland <span class="filter-list-count">(909)</span></a></li>            
        </ul>

        <hr>

    
    </form>
</div><!-- /.filter -->




    <div class="panels-highlighted">
    <div class="row">
        <div class="panel-highlighted-wrapper col-sm-6">
            <div class="panel-highlighted-inner panel-highlighted-secondary">
                <h2>TheCustomWriter Pre-requisite Requirements:</h2>

                <p>
                   <ul style="color:#fff">
                       <li><i class="fa fa-check"></i>Must possess at least a Degree in your field of specialization. We will request a certified copy of the same during application.</li>
                       <li><i class="fa fa-check"></i>You must possess exceptional academic writing experience of not less than 2 years</li>
                       <li><i class="fa fa-check"></i>You must possess knowledge of various citation styles including but not limited to: MLA, APA. Harvard, Chicago, Turabian and AMA.</li>
                       <li><i class="fa fa-check"></i>We expect our writers to be available and reachable for prompt communication.</li>
                       <li><i class="fa fa-check"></i>Deadlines must be met, otherwise fines will be applied and you may lose your writing privileges with us.</li>

                   </ul>
                </p>

              
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->

        <div class="panel-highlighted-wrapper col-sm-6">
            <div class="panel-highlighted-inner panel-highlighted-primary panel">
                 <h2>Our Benefits</h2>

                <p>
                   <ul style="color:#fff">
                       <li><i class="fa fa-check"></i>No Registration/Sign Up Fees</li>
                       <li><i class="fa fa-check"></i>24/7 Responsive Writer Support</li>
                       <li><i class="fa fa-check"></i>Great Mentoring Program</li>
                       <li><i class="fa fa-check"></i>WRegular and Secure Payment Processing</li>
                       <li><i class="fa fa-check"></i>Best Pay Rates in the Business</li>

                   </ul>
                </p>
                <p>Join our team of top writers and get the best payment and support on the market. We are always looking for new team members, so sign up today and start earning money for your writing! It's easy and free to join.</p>

                <a href="{{url('register')}}" class="btn btn-white">Create an account now</a>
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->
    </div><!-- /.row-->
</div><!-- /.panels -->


    <div class="panels">
    <div class="row">
        <div class="panel-wrapper col-sm-4">
            <div class="panel-inner">
                <h2>Why Profession?</h2>

                <ul>
                    <li><i class="fa fa-check"></i> Clean &amp; Corporate webdesign</li>
                    <li><i class="fa fa-check"></i> Well coded HTML template</li>
                    <li><i class="fa fa-check"></i> Multiple Layout Options</li>
                </ul>
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->

        <div class="panel-wrapper col-sm-4">
            <div class="panel-inner">
                <h2>Main Features</h2>

                <ul>
                    <li><i class="fa fa-check"></i> Bootstrap 3.x &amp; SASS</li>
                    <li><i class="fa fa-check"></i> Super lighting page speed</li>  
                    <li><i class="fa fa-check"></i> Well documented code</li>
                </ul>  
            </div><!-- /.panel-inner -->  
        </div><!-- /.panel-wrapper -->

        <div class="panel-wrapper col-sm-4">
            <div class="panel-inner">
                <h2>Transparent Pricing</h2>

                <p>Are you intersted in our services? Don't hesitate and contact us.</p>
                <a href="pricing.html" class="panel-show-more">Check Pricing</a>
            </div><!-- /.panel-inner --> 
        </div><!-- /.panel-wrapper -->
    </div><!-- /.row -->
</div><!-- /.panels -->

    <div class="block background-secondary fullwidth candidate-title">
    <div class="page-title">
        <h2>Find Your Best Candidate</h2>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <p>
                    Donec tincidunt felis quam, eu tempus purus finibus in. Curabitur hendrerit, odio in viverra interdum, lorem velit scelerisque ipsum, a sagittis ligula leo in dolor. Etiam vestibulum.
                </p>
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.page-title -->
</div><!-- /.fullwidth -->



 <div class="filter">
    <h2>Top Orders by Discipline</h2>
    <form method="get" action="">
      

        <ul class="filter-list">
            <li><a href="positions.html">Alabama <span class="filter-list-count">(1 704)</span></a></li>
            <li><a href="positions.html">Alaska <span class="filter-list-count">(1 215)</span></a></li>
            <li><a href="positions.html">Arizona <span class="filter-list-count">(904)</span></a></li>
            <li><a href="positions.html">Arkansas <span class="filter-list-count">(2 744)</span></a></li>
            <li><a href="positions.html">California <span class="filter-list-count">(904)</span></a></li>
            <li><a href="positions.html">Colorado <span class="filter-list-count">(1 804)</span></a></li>
            <li><a href="positions.html">Connecticut <span class="filter-list-count">(875)</span></a></li>
            <li><a href="positions.html">Delaware <span class="filter-list-count">(546)</span></a></li>
            <li><a href="positions.html">Florida <span class="filter-list-count">(7 895)</span></a></li>
            <li><a href="positions.html">Georgia <span class="filter-list-count">(4 562)</span></a></li>
            <li><a href="positions.html">Hawaii <span class="filter-list-count">(1 658)</span></a></li>
            <li><a href="positions.html">Idaho <span class="filter-list-count">(1 354)</span></a></li>
            <li><a href="positions.html">Illinois <span class="filter-list-count">(6 988)</span></a></li>
            <li><a href="positions.html">Indiana <span class="filter-list-count">(1 235)</span></a></li>
            <li><a href="positions.html">Iowa <span class="filter-list-count">(4 563)</span></a></li>
            <li><a href="positions.html">Kansas <span class="filter-list-count">(1 549)</span></a></li>

            <li><a href="positions.html">Kentucky <span class="filter-list-count">(432)</span></a></li>
            <li><a href="positions.html">Louisiana <span class="filter-list-count">(4 123)</span></a></li>
            <li><a href="positions.html">Maine <span class="filter-list-count">(923)</span></a></li>
            <li><a href="positions.html">Maryland <span class="filter-list-count">(909)</span></a></li>            
        </ul>

        <hr>

    
    </form>
</div><!-- /.filter -->

  

</div><!-- /.container -->














        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@endsection
