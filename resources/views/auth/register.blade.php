@extends('layouts.landing')

@section('content')
        <div class="main-wrapper">
        <div class="main">
            <div class="background-secondary fullwidth document-title">
                <div class="container">
                    <h1 class="center" style="color:#fff">Register as an Academic Writer:</h1>
                </div><!-- /.container -->
            </div><!-- /.document-title -->

<div class="container" style="background:#F0FFFF">
    <div class="col-sm-9">
        <form role="form" method="POST" action="{{ url('/register') }}" enctype="multipart/form-data">
        {{ csrf_field() }}

         <h3 class="page-header">Personal Information</h3>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control">
                         @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label>First Name</label>
                        <input type="text" name="firstname" class="form-control">
                         @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <label>Last Name</label>
                        <input type="text" name="lastname" class="form-control">
                         @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                     <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control">
                         @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                        <label>Country</label>
                        <input type="text" name="country" class="form-control">
                         @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif

                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <label>City</label>
                        <input type="text" name="city" class="form-control">
                         @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                        <label>ZIP</label>
                        <input type="text" name="zip" class="form-control">
                         @if ($errors->has('zip'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zip') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label>Mobile Phone</label>
                        <input type="text" name="mobile_number" class="form-control">
                        @if ($errors->has('mobile_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label>Worked for other online academic assistance companies?</label>
                       <div class="radio">
                      <label><input type="radio" name="employed" value="yes" onclick="if (this.checked) { jQuery('#othervar_employed').show(); }" onblur="if (this.checked) { jQuery('#othervar_employed').show(); }" onfocus="if (this.checked) { jQuery('#othervar_employed').show(); }">Yes</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" value="no" name="employed" onclick="if (this.checked) { jQuery('#othervar_employed').hide(); jQuery('#id_employed_txt').val(''); }" onblur="if (this.checked) { jQuery('#othervar_employed').hide(); jQuery('#id_employed_txt').val(''); }" onfocus="if (this.checked) { jQuery('#othervar_employed').hide(); jQuery('#id_employed_txt').val(''); }" type="radio"></label>
                      <label for="employed_n">No <br></label>
                    <div id="othervar_employed" style="clear:both; display: none;">Other companies worked for:&nbsp;<input name="employed_txt" id="id_employed_txt" maxlength="255" style="width: 100px;" type="text"></div><span id="error_container_employed"></span>
                    </div>

                    </div><!-- /.form-group -->
                     <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label>Choose A password</label>
                        <input type="password" name="password" class="form-control">
                         @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label>Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control">
                         @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                  
                </div><!-- /.col-* -->
            </div><!-- /.row -->

            <h3 class="page-header">Citations & Disciplines</h3>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>What citation styles are you familiar with?</label>
                        <div class="row">
                        <div class="col-sm-4">
                        <label class="checkbox-inline" value="MLA" style="margin-right:10px"><input type="checkbox" value="MLA" name="citation[]" >MLA</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" value="APA" style="margin-right:10px"><input type="checkbox" value="APA" name="citation[]">APA</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" value="Chicago / Turabia"style="margin-right:10px"><input type="checkbox" value="Chicago / Turabia" name="citation[]">Chicago / Turabia</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" value="Harvard" style="margin-right:10px"><input type="checkbox" value="Harvard" name="citation[]">Harvard</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Other" name="citation[]" onclick="if (this.checked) { jQuery('#othervar_citation').show(); } else { jQuery('#othervar_citation').hide(); jQuery('#id_citation_txt').val(''); }" onblur="if (this.checked) { jQuery('#othervar_citation').show(); } else { jQuery('#othervar_citation').hide(); jQuery('#id_citation_txt').val(''); }" onfocus="if (this.checked) { jQuery('#othervar_citation').show(); } else { jQuery('#othervar_citation').hide(); jQuery('#id_citation_txt').val(''); }" type="checkbox"></label>
                        <label for="citation_5">Other</label></div>
                        <div id="othervar_citation" style="clear:both; display: none;">Please specify styles:&nbsp;<input name="citation_txt" id="id_citation_txt" maxlength="255" style="width: 200px;" type="text"></div>
                        <span id="error_container_citation"></span>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
                <div class="col-sm-12">
                    <div class="form-group" style="padding:50px">
                        <label>Academic Disciplines: *  
Up to 5 academic disciplines</label>
                        <div class="row">
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Accounting" name="academic_discipline[]">Accounting</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Anthropology" name="academic_discipline[]">Anthropology</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Application Letters" name="academic_discipline[]">Application Letters</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Art & Architecture" name="academic_discipline[]">Art & Architecture</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Biology" name="academic_discipline[]">Biology</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value=" Business" name="academic_discipline[]"> Business</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Chemistry" name="academic_discipline[]">Chemistry</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Classics" name="academic_discipline[]">Classics</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Communications" name="academic_discipline[]">Communications</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Computer science" name="academic_discipline[]">Computer science</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Economics Education" name="academic_discipline[]">Economics Education</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="English literature" name="academic_discipline[]">English literature</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Family & consumer science" name="academic_discipline[]">Family & consumer science</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Film & Theater studies" name="academic_discipline[]">Film & Theater studies</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Finance" name="academic_discipline[]">Finance</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="History" name="academic_discipline[]">History</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Law" name="academic_discipline[]">Law</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Management" name="academic_discipline[]">Management</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Marketing" name="academic_discipline[]">Marketing</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Mathematics" name="academic_discipline[]">Mathematics</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Medicine" name="academic_discipline[]">Medicine</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Music" name="academic_discipline[]">Music</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Nursing" name="academic_discipline[]">Nursing</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Philosophy" name="academic_discipline[]">Philosophy</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Physics" name="academic_discipline[]">Physics</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Political science" name="academic_discipline[]" >Political science</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="Psychology" name="academic_discipline[]">Psychology</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Religious studies" name="academic_discipline[]">Religious studies</label>
                        </div>
                        <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Shakespeare" name="academic_discipline[]">Shakespeare</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Sociology" name="academic_discipline[]">Sociology</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Statistics" name="academic_discipline[]">Statistics</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Technology" name="academic_discipline[]">Technology</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Web, High tech" name="academic_discipline[]">Web, High tech</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Women's & gender studies " name="academic_discipline[]">Women's & gender studies </label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" value="World Affairs" name="academic_discipline[]">World Affairs</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="World literature" name="academic_discipline[]">World literature</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="Zoology" name="academic_discipline[]">Zoology</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline"  style="margin-right:10px"><input type="checkbox" value="General" name="academic_discipline[]">General</label>
                        </div>
                         <div class="col-sm-4">
                        <label class="checkbox-inline" style="margin-right:10px"><input type="checkbox" name="academic_discipline[]" onClick="if (this.checked) { jQuery('#other_academic_discipline').show(); } else { jQuery('#other_academic_discipline').hide(); jQuery('#id_academic_discipline').val(''); }" onBlur="if (this.checked) { jQuery('#other_academic_discipline').show(); } else { jQuery('#other_academic_discipline').hide(); jQuery('#id_academic_discipline').val(''); }" onFocus="if (this.checked) { jQuery('#other_academic_discipline').show(); } else { jQuery('#other_academic_discipline').hide(); jQuery('#id_academic_discipline').val(''); }" /></label>
                        <label for="academic_discipline_39">Other</label></div>
<div id="other_academic_discipline" style="clear:both; display: none;">Other disciplines:&nbsp;<input type="text" name="discipline_txt" id="id_discipline_txt" maxlength="250" value="" style="width:230px;" />
                        </div>
                        @if ($errors->has('academic_discipline'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('academic_discipline') }}</strong>
                                    </span>
                                @endif

                        </div>
                        

                       
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

            </div><!-- /.row -->

             <h3 class="page-header">CV details</h3>
            <div class="row">
                <div class="col-sm-6">
           
            <div class="form-group">
                        <label>Your native language?</label>
                        <input type="text" name="language" class="form-control">
                        @if ($errors->has('language'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('language') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label>Highest verifiable academic degree:</label>
                        <input type="text" name="degree" class="form-control">
                        @if ($errors->has('degree'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('degree') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                     <div class="form-group">
                        <label form="form-register-photo">Your detailed CV:</label>
                        <input type="file" name="detailed_cv" id="detailed_cv">
                        @if ($errors->has('detailed_cv'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('detailed_cv') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group-->
                    </div>
                    <div class="col-sm-6">
            <div class="form-group">
            <label>Tell us more about your experience, i.e your most competent areas and level of proficiency::</label>
                <textarea id="text" name="brief_cv" class="form-control"></textarea>
                @if ($errors->has('brief_cv'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('brief_cv') }}</strong>
                                    </span>
                                @endif
            </div><!-- /.form-group -->

            </div>
         </div>   
            <h3 class="page-header">Certification and Identification</h3>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Give your Certificate a title</label>
                        <input type="text" name="cert_title" class="form-control">
                        @if ($errors->has('cert_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cert_title') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                     <div class="form-group">
                        <label form="form-register-photo">Upload your cert in image format::</label>
                        <input type="file" name="cert_file_one" id="cert_file_one">
                        @if ($errors->has('cert_file_one'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cert_file_one') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group-->
  
                </div><!-- /.col-* -->

                 <div class="col-sm-6">
                    <div class="form-group">
                        <label>Give your National ID a title:</label>
                        <input type="text" name="id_title" class="form-control">
                        @if ($errors->has('id_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_title') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                     <div class="form-group">
                        <label form="form-register-photo">Upload a photo of your ID</label>
                        <input type="file" name="id_image_one" id="id_image_one">
                         @if ($errors->has('id_image_one'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_image_one') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group-->
  
                </div><!-- /.col-* -->
            </div>

              

            <h3 class="page-header">Sample Essays </h3>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Title for sample essay One: </label>
                        <input type="text" name="title_sample_one"class="form-control">
                        @if ($errors->has('title_sample_one'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_sample_one') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                     <div class="form-group">
                        <label form="form-register-photo">Upload sample essay One:</label>
                        <input type="file" name="sample_one_file" id="sample_one_file">
                        @if ($errors->has('sample_one_file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sample_one_file') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group-->
  
                </div><!-- /.col-* -->

                 <div class="col-sm-6">
                    <div class="form-group">
                        <label>Title for Sample essay Two</label>
                        <input type="text" name="title_sample_two" class="form-control">
                        @if ($errors->has('title_sample_two'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_sample_two') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                     <div class="form-group">
                        <label form="form-register-photo">Upload sample essay Two</label>
                        <input type="file" name="sample_two_file" id="sample_two_file">
                        @if ($errors->has('sample_two_file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sample_two_file') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group-->
  
                </div><!-- /.col-* -->
            </div>

            <h3 class="page-header">Payments & Terms </h3>

            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Preferred payment method: </label>
                        <input type="text" name="payment_method" class="form-control">
                         @if ($errors->has('payment_method'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('payment_method') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->

                    <div class="form-group">
                       <input type="checkbox" value="" id="terms" name="terms">I accept terms and conditions
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->

                <div class="col-sm-7">
                    <div class="form-group">
                        <label>Payment Details</label>
                        <textarea class="form-control" name="payment_details" rows="5"></textarea>
                        @if ($errors->has('payment_details'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('payment_details') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->

            <hr>

            <div class="center">
                <button type="submit" class="btn btn-secondary btn-lg">Create Account</button>
            </div><!-- /.center -->
        </form>
    </div><!-- /.col-* -->

    <div class="col-sm-3">
        <div class="widget">
          <img src="landing/assets/img/reg.png" alt="create an account">
        </div><!-- /.widget -->

        <div class="widget">
            <h2>Registration Benefits</h2>

            <ul class="nav">
                <li><a href="#"><span>1.</span> Basic Information</a></li>
                <li><a href="#"><span>2.</span> Contact</a></li>
                <li><a href="#"><span>3.</span> Biography</a></li>
                <li><a href="#"><span>4.</span> Experience</a></li>
                <li><a href="#"><span>5.</span> Education </a></li>
            </ul>
        </div><!-- /.widget -->

        <div class="widget">
            <h2>Do you have an account?</h2>

            <p>If you don't have an account and you are looking for a writing job. Feel free to <a href="{{url('register')}}">create new account</a> for companies with transparent pricing.</p>
        </div><!-- /.widget -->
    </div><!-- /.col-* -->
</div><!-- /.container -->

        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@endsection
