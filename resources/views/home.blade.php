@extends('layouts.admin')

@section('content')
 <div id="page_content">
        <div id="page_content_inner">
          <!-- statistics (small charts) -->
          <div class="uk-alert uk-alert-info">
            <strong>Dear User!</strong><br>
            Kindly note that this month of August we shall be on vacation. However, we shall still have A FEW orders that will be preserved for the most cooperative and reliable writers. Please keep checking in and if you don't find enough jobs, don't worry because from 1st September, we shall be back with a lot of work. Regards Admin.
        </div>

<div class="md-card uk-margin-medium-bottom">
        <div class="uk-grid uk-grid-divider" data-uk-grid-margin>
                        <div class="uk-width-large-1-3 uk-width-medium-1-2">
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-text-success uk-icon-file"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Orders</span>
                                        <a href="{{URL::route('new_order')}}"><span class="uk-text-small" style="color:#0097a7"><i class="uk-icon-plus-circle"></i>Create Order.</span></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-text-success uk-icon-legal"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Users</span>
                                       <a href="{{URL::route('create_user')}}"><span class="uk-text-small" style="color:#0097a7"><i class="uk-icon-plus-circle"></i>Create User.</span></a>
                                    </div>
                                </li>
                               
                            </ul>
                        </div>
                       

                      <div class="uk-width-large-1-3 uk-width-medium-1-2">
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE0B9;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Messages</span>
                                       <a href="{{URL::route('messages')}}"><span class="uk-text-small" style="color:#0097a7"><i class="uk-icon-plus-circle"></i>New Message.</span></a>
                                    </div>
                                </li>
                                 <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-text-success uk-icon-users"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Payments</span>
                                        <a href="{{URL::route('payments')}}"><span class="uk-text-small" style="color:#0097a7"><i class="uk-icon-plus-circle"></i>Make Payment.</span></a>
                                    </div>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                    </div>



                      <!-- add order modal -->

         <div class="uk-modal" id="modal_order">
          <div class="uk-modal-dialog">
           <button type="button" class="uk-modal-close uk-close"></button>
          <div class="uk-modal-header">
           <h3 class="uk-modal-title">Add New Order <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i></h3>
        </div>
                     
          <div class="md-card">
            <div class="md-card-content large-padding">
              <form id="form_validation" class="uk-form-stacked" method="POST" action="{{URL::route('new_order')}}">
              {{ csrf_field() }}
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Work Type<span class="req">*</span></label>
                      <select id="select" data-md-selectize data-md-selectize-bottom>
                                <option value="">Select...</option>
                                <option value="a">Item A</option>
                                <option value="b">Item B</option>
                                <option value="c">Item C</option>
                            </select>
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">Title<span class="req">*</span></label>
                      <input type="email" name="email" data-parsley-trigger="change" required  class="md-input" />
                    </div>
                  </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                 <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Level<span class="req">*</span></label>
                      <input type="text" name="mobile_number" required class="md-input" />
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Max Bid<span class="req">(optional)</span></label>
                      <input type="text" name="company_name" class="md-input" />
                    </div>
                  </div>
                 
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Number of pages/ Words:<span class="req">*</span></label>
                      <input type="text" name="address" required class="md-input" />
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Spacing<span class="req">*</span></label>
                      <input type="text" name="national_id" required class="md-input" />
                    </div>
                  </div>
                   
                </div>
                 <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Provide digital sources used:<span class="req">*</span></label>
                      <input type="text" name="address" required class="md-input" />
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Subject or Discipline<span class="req">*</span></label>
                      <input type="text" name="national_id" required class="md-input" />
                    </div>
                  </div>
                   
                </div>
                 <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Deadline date: (Select Urgency):<span class="req">*</span></label>
                      <input type="text" name="address" required class="md-input" />
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Order Price ( KES)<span class="req">*</span></label>
                      <input type="text" name="national_id" required class="md-input" />
                    </div>
                  </div>
                   
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Track Order Id:<span class="req">*</span></label>
                      <input type="text" name="address" required class="md-input" />
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Citation Style<span class="req">*</span></label>
                      <input type="text" name="national_id" required class="md-input" />
                    </div>
                  </div>
                   
                </div>
                 <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Number of sources:<span class="req">*</span></label>
                      <input type="text" name="address" required class="md-input" />
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Order instructions:<span class="req">*</span></label>
                      <textarea name="national_id" required class="md-input"></textarea>
                    </div>
                  </div>
                   
                </div>
                <p>If you wish to upload files, Please do so on 'Add Files' link on Manage Order section.</p>
               
                <div class="uk-grid">
                  <div class="uk-width-1-1">
                    <button type="submit" class="md-btn md-btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
         </div>
        </div>


       <!-- end add order modal -->





<h3 class="heading_b uk-margin-bottom">Manage Orders <a style="font-size:14px; margin-left:20px" href="{{URL::route('new_order')}}">create new order>>>></a></h3>


        <div class="md-card">

            <div class="md-card-content">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin>

                    <div class="uk-width-medium-1-12">
                        
                        <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#switcher-content-a-fade', animation: 'fade'}">
                            <li><a class= uk-active" href="#">All</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Available</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Pending</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Confirmed</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Unconfirmed</a></li>
                            <li><a class="md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Revision</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Editing</a></li>
                            <li><a class="md-btn md-btn-success md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Completed</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Approved</a></li>
                            <li><a class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Rejected</a></li>
                          
                        </ul>
                        <ul id="switcher-content-a-fade" class="uk-switcher uk-margin">
                            <li>
                              <h5 class="heading_c uk-margin-bottom">All Orders</h5>
                              
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                       
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            <th>Assigned?</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                           
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            <th>Assigned?</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($orders as $order)
                        <tr>
                          
                            <td><a href="{{ URL::route('view_order', $order->ordersid) }}">{{$order->ordersid}}</a></td>
                            <td>{{$order->firstname}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                              <td class="uk-text-center">Not Yet</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                            <li>
                        <h5 class="heading_c uk-margin-bottom">Available Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                            <li>
                            <h5 class="heading_c uk-margin-bottom">Pending Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Confirmed Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Unconfirmed Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Orders Under Revision</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                             <h5 class="heading_c uk-margin-bottom">Orders Under Editing</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Completed Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Approved Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Rejected Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                        </ul>
                    </div>
                  </div>
                </div>
              </div>


        </div>
    </div>


@endsection
 @section('page-script')
<!-- page specific plugins -->
   {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    @stop
