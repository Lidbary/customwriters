@extends('layouts.admin')

@section('content')
 <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-8-10 uk-container-center">
                <div class="uk-clearfix">
                   
                   
                </div>
            </div>
        </div>
    </div>
    <div id="page_content">
        <div id="page_content_inner">
             @if(Session::has('success_message'))
                                              <div class="uk-alert uk-alert-success">{!! session('success_message') !!}</div>
                                             @endif
            <div class="md-card-list-wrapper" id="mailbox">
                <div class="uk-width-large-8-10 uk-container-center">
                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Today</div>
                        <div class="md-card-list-header md-card-list-header-combined heading_list" style="display: none">All Messages</div>
                        <ul class="hierarchical_slide">

                        @foreach ($messages as $messag)
                            <li>
                                <div class="md-card-list-item-menu" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                    <a href="#" class="md-icon material-icons">&#xE5D4;</a>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="#"><i class="material-icons">&#xE15E;</i> Reply</a></li>
                                            <li><a href="#"><i class="material-icons">&#xE149;</i> Archive</a></li>
                                            <li><a href="#"><i class="material-icons">&#xE872;</i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="md-card-list-item-date">16 May</span>
                                <div class="md-card-list-item-select">
                                    <input type="checkbox" data-md-icheck />
                                </div>
                              
                                <div class="md-card-list-item-sender">
                                    <span><p style="color:#39f">{{$messag->email}}</p></span>
                                </div>
                                <div class="md-card-list-item-subject">
                                   
                                    <span>{{$messag->subject}}</span>
                                </div>
                                <div class="md-card-list-item-content-wrapper">
                                    <div class="md-card-list-item-content">
                                    {{$messag->message}}<br>

                                    <h4>Attached Files</h4>

                                    <ul>

                                        <li><a href="/uploads/message_files/{{$messag->message_file}}" download="{{$messag->message_file}}">{{$messag->message_file}}</a></li>
                                    </ul>
                                    </div>
                                    <form class="md-card-list-item-reply">
                                        <label for="mailbox_reply_1629">Reply to</label>
                                    <input type="text" name="user_id" value={{$messag->email}} class="md-input">
                                     @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                     </span>
                                            @endif
                                        <textarea class="md-input md-input-full" name="mailbox_reply_1629" id="mailbox_reply_1629" cols="30" rows="4"></textarea>
                                        <button class="md-btn md-btn-flat md-btn-flat-primary">Send</button>
                                    </form>
                                </div>
                            </li>
                          @endforeach
                          
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave" href="#mailbox_new_message" data-uk-modal="{center:true}">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div>

    <div class="uk-modal" id="mailbox_new_message">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close uk-close" type="button"></button>
            <form method="POST" action="{{URL::route('new_message')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Compose Message</h3>
                </div>
                <div class="uk-margin-medium-bottom">
                     <div class="parsley-row">
                      <label for="to">To</label>
                      <select class="md-input" name="user_id" required>
                            <option value="" disabled selected>Select...</option>
                            @foreach ($users as $user)
                            <option value="{{$user->id}}">{{$user->firstname}} {{$user->lastname}}</option>
                            @endforeach
                            
                          </select>
                    </div>
                  
                </div>
                <div class="uk-margin-medium-bottom">
                     <div class="parsley-row">
                      <label for="to">Subject</label>
                       <input type="text" name="subject" class="md-input">
                         @if ($errors->has('subject'))
                            <span class="help-block">
                            <strong>{{ $errors->first('subject') }}</strong>
                         </span>
                                @endif
                    </div>
                  
                </div>
                <div class="uk-margin-large-bottom">
                    <label for="mail_new_message">Message</label>
                    <textarea name="message" id="mail_new_message" cols="30" rows="6" class="md-input"></textarea>
                </div>
                <div id="mail_upload-drop" class="uk-file-upload">
                    <label form="form-register-photo">Upload Files:</label>
                        <input type="file" name="message_file" id="file_name">
                        @if ($errors->has('message_file'))
                        <span class="help-block">
                    <strong><a href="" >{{ $errors->first('message_file') }}</strong>
                    </span>
                    @endif
                </div>
                <div id="mail_progressbar" class="uk-progress uk-hidden">
                    <div class="uk-progress-bar" style="width:0">0%</div>
                </div>
                <div class="uk-modal-footer">
                    <a href="#" class="md-icon-btn"><i class="md-icon material-icons">&#xE226;</i></a>
                    <button type="submit" class="uk-float-right md-btn md-btn-flat md-btn-flat-primary">Send</button>
                </div>
            </form>
        </div>
    </div>

@endsection
 @section('page-script')

   {!! Html::script('admin/assets/js/pages/page_mailbox.min.js') !!}

    @stop
