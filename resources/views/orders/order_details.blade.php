@extends('layouts.admin')

@section('content')

<div id="page_content">
        <div id="page_content_inner">
         @if(Session::has('success_message'))
                                              <div class="uk-alert uk-alert-success">{!! session('success_message') !!}</div>
                                             @endif
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_menu" data-uk-dropdown="{pos:'left-top'}">
                                <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="user_heading_avatar">
                               
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom">
                                <span class="uk-text-truncate">Order # {{$vieworder->id}} </span>
                                <span class="sub-heading">Status: <span style="color:orange">Available</span></span></h2>
                                <span class="sub-heading">Deadline ({{$vieworder->deadline}})</span>
                             
                            </div>
                           
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">About</a></li>
                                <li><a href="#">Upload Files</a></li>
                                <li><a href="#">Assign Order </a></li>
                                
                            </ul>
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                   <p>{{$vieworder->instructions}}</p>                             
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Order Details</h4>
                                            <ul class="md-list md-list-addon">
                                               
                                                <li>
                                                   
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Title</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->order_title}}</span>
                                                    </div>
                                                </li>
                                                  <li>
                                                   
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Provide sources used?</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->digital_sources}}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                  
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Paper type</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->doctype}}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                   
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Paper format</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->citation}}</span>
                                                    </div>
                                                </li>
                                                 <li>
                                                   
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Normal/Silver/Advanced</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->max_bid}}</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">More Details</h4>
                                            <ul class="md-list">
                                                <li>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Course level</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->order_level}}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Subject Area</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->discipline}}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2"># pages</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->no_of_pages}}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2">Spacing</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->spacing}}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading" style="color:#1976d2"># sources</span>
                                                        <span class="uk-text-small uk-text-muted">{{$vieworder->no_of_sources}}</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <h4 class="heading_c uk-margin-bottom">Uploaded files</h4>
                                    <div class="timeline">
                                        
                                        <div class="timeline_item">
                                    click on the link to download file
                                    <ul>
                                    @foreach ($files as $file)
                                        <li><a href="/uploads/order_files/{{$file->name}}" download="{{$file->name}}">{{$file->name}}</a> Uploaded by </li>
                                        @endforeach
                                    </ul>
                                     
                                    </div>
                                </li>
                                <li>
                                    <div id="user_profile_gallery" data-uk-check-display class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4" data-uk-grid="{gutter: 4}">
                                        <div>
                                            <a href="assets/img/gallery/Image01.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image01.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image02.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image02.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image03.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image03.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image04.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image04.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image05.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image05.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image06.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image06.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image07.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image07.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image08.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image08.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image09.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image09.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image10.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image10.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image11.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image11.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image12.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image12.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image13.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image13.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image14.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image14.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image15.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image15.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image16.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image16.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image17.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image17.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image18.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image18.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image19.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image19.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image20.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image20.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image21.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image21.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image22.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image22.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image23.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image23.jpg" alt=""/>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="assets/img/gallery/Image24.jpg" data-uk-lightbox="{group:'user-photos'}">
                                                <img src="assets/img/gallery/Image24.jpg" alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                    <ul class="uk-pagination uk-margin-large-top">
                                        <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                                        <li class="uk-active"><span>1</span></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><span>&hellip;</span></li>
                                        <li><a href="#">20</a></li>
                                        <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <ul class="md-list">
                                      
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><a href="#">Quibusdam unde numquam exercitationem delectus est quos illum.</a></span>
                                                <div class="uk-margin-small-top">
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">18 May 2016</span>
                                                </span>
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">26</span>
                                                </span>
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">530</span>
                                                </span>
                                                </div>
                                            </div>
                                        </li>
                                      
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><a href="#">Sed sit quidem nostrum eligendi.</a></span>
                                                <div class="uk-margin-small-top">
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">23 May 2016</span>
                                                </span>
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">15</span>
                                                </span>
                                                <span class="uk-margin-right">
                                                    <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">877</span>
                                                </span>
                                                </div>
                                            </div>
                                        </li>


                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-content">
                            
                                         
                                             <fieldset style="border:1px dotted #0000bb">
                                            <legend><p class="heading_a"> Upload Files</p></legend>
                                            <div class="uk-grid">
                                           
                                                <div class="uk-width-1-1">
                                                <form method="POST" action="{{URL::route('upload_file')}}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                   <div class="form-group">
                                                    <label form="form-register-photo">Upload Files:</label>
                                                    <input type="file" name="name" id="file_name">
                                                    @if ($errors->has('order_file'))
                                                    <span class="help-block">
                                                    <strong><a href="" >{{ $errors->first('order_file') }}</strong>
                                                    </span>
                                                    @endif
                                                </div><!-- /.form-group-->
                                                <input type="hidden" value="{{$vieworder->id}}" name="order_id">
                                                 <div class="uk-grid"  data-uk-grid-margin style="margin-top:30px">
                                                  <div class="uk-width-1-1">
                                                     <button type="submit" class="md-btn md-btn-primary">Upload File</button>
                                                  </div>
                                                </div>
                                                </form>
                                                   
                                                </div>
                                            </div>
                                            </fieldset>

                                         
                             <fieldset style="border:1px dotted #0000bb">
                                <legend><p class="heading_a">Assign order to any writer</p></legend>
                                <div class="uk-grid" data-uk-grid-margin>
                                   
                                    <div class="uk-width-medium-1-1">

                                 <form method="POST" action="{{URL::route('assign_order')}}">
                                                {{ csrf_field() }}
                                                   <div class="form-group">
                                                    <select id="select_demo_5" name="user_id" data-md-selectize data-md-selectize-bottom>
                                                    <option value="">Select...</option>
                                                    @foreach ($writers as $writer)
                                                    <option value="{{$writer->user_id}}">{{$writer->firstname}}</option>
                                                  @endforeach
                                                </select>
                                                </div><!-- /.form-group-->
                                                <input type="hidden" value="{{$vieworder->id}}" name="order_id">
                                                 <div class="uk-grid"  data-uk-grid-margin style="margin-top:30px">
                                                  <div class="uk-width-1-1">
                                                     <button type="submit" class="md-btn md-btn-primary">Assign Order</button>
                                                  </div>
                                                </div>
                                                </form>
                           
                           
                        </div>
                     
                    </div>
                     </fieldset>
               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 

@endsection
 @section('page-script')
    {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    {!! Html::script('admin/assets/js/pages/forms_file_upload.min.js') !!}
    @stop
