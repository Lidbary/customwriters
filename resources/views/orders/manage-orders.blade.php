@extends('layouts.admin')

@section('content')
 <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Manage Orders <a style="font-size:14px; margin-left:20px" href="{{URL::route('new_order')}}">create new order>>>></a></h3>


        <div class="md-card">

            <div class="md-card-content">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin>

                    <div class="uk-width-medium-1-12">
                        
                        <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#switcher-content-a-fade', animation: 'fade'}">
                            <li><a class= uk-active" href="#">All</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Available</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Pending</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Confirmed</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Unconfirmed</a></li>
                            <li><a class="md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Revision</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Editing</a></li>
                            <li><a class="md-btn md-btn-success md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Completed</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Approved</a></li>
                            <li><a class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Rejected</a></li>
                          
                        </ul>
                        <ul id="switcher-content-a-fade" class="uk-switcher uk-margin">
                            <li>
                              <h5 class="heading_c uk-margin-bottom">All Orders</h5>
                              
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                       
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            <th>Assigned?</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                           
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            <th>Assigned?</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($orders as $order)
                        <tr>
                          
                            <td><a href="{{ URL::route('view_order', $order->ordersid) }}">{{$order->ordersid}}</a></td>
                            <td>{{$order->firstname}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                              <td class="uk-text-center">Not Yet</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                            <li>
                        <h5 class="heading_c uk-margin-bottom">Available Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                            <li>
                            <h5 class="heading_c uk-margin-bottom">Pending Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Confirmed Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Unconfirmed Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Orders Under Revision</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                             <h5 class="heading_c uk-margin-bottom">Orders Under Editing</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Completed Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Approved Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                             <li>
                            <h5 class="heading_c uk-margin-bottom">Rejected Orders</h5>                     
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr style="background:#eeeeee">
                        <th>Actions</th>
                            <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            

                        </tr>
                        </thead>

                        <tfoot>
                        <tr style="background:#eeeeee">
                            <th>Actions</th>
                             <th>Order No</th>
                            <th>Client</span></th>
                            <th>Created</th>
                            <th>Track</th>
                            <th>Due</th>
                            <th>Status</th>
                            <th>Pages</th>
                            <th>Cost</th>
                            
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach ($available as $order)
                        <tr>
                          <td class="uk-text-center">
                                 <div class="uk-button-dropdown" data-uk-dropdown>
                                <a>Action <i class="material-icons">&#xE313;</i></a>
                                 <div class="uk-dropdown uk-dropdown-small uk-dropdown-scrollable">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a href="#">Make Available</a></li>
                                        <li><a href="#">Mark as Done</a></li>
                                        <li><a href="#">Return to Editing</a></li>
                                        <li><a href="#">Un Approve</a></li>
                                        <li><a href="#">Remove</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Edit</a></li>  
                                    </ul>
                                </div>
                            </div>       
                            </td>
                            <td>{{$order->id}}</td>
                            <td>walter</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td>{{$order->track_id}}</td>
                            <td><span style="color:#2196f3">{{$order->created_at}}</span></td>
                             <td><span style="color:#2196f3">approved</span></td>
                             <td><span style="color:#2196f3">{{$order->no_of_pages}}</span></td>
                              <td>{{$order->client_price}}</td>
                            
                            
                           
                            
                           
                           
                            
                           
                          
                        </tr>
                        @endforeach
                         

                         

                        </tbody>
                    </table>
                </div>
            </div>
                            </li>
                        </ul>
                    </div>
                  </div>
                </div>
              </div>



        </div>
    </div>


@endsection
 @section('page-script')
    {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    @stop
