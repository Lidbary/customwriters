<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'firstname', 'lastname', 'username', 'country', 'city', 'zip', 'employed', 'citation', 'academic_discipline', 'language', 'degree', 'detailed_cv', 'brief_cv', 'cert_title', 'cert_file_one', 'id_title', 'id_image_one', 'title_sample_one', 'sample_one_file', 'title_sample_two',  'sample_two_file', 'payment_method', 'payment_details', 'mobile_number', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRole($roles) 
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }

        return false;

    }

    public function hasRole($role) 
    {
        if ($this->roles()->where('name', $role)->first()) 
        {
            return true;
        }

        return false;
    }

     public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
