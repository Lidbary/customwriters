<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'username' => 'required|max:255',
            'mobile_number' => 'required|max:255',
            'employed' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            'zip' => 'required|max:255',
            'citation' => 'required|max:255',
            'academic_discipline' => 'required|max:255',
            'language' => 'required|max:255',
            'degree' => 'required|max:255',
            'detailed_cv' => 'required',
            'brief_cv' => 'required',
            'cert_title' => 'required|max:255',
            'cert_file_one' => 'required',
            'id_title' => 'required|max:255',
            'id_image_one' => 'required',
            'title_sample_one' => 'required|max:255',
            'sample_one_file' => 'required',
            'title_sample_two' => 'required|max:255',
            'sample_two_file' => 'required',
            'payment_method' => 'required|max:255',
            'payment_details' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $cv = $data['detailed_cv'];

        $destinationPath = 'uploads/detailed_cv';
        $extension = $cv->getClientOriginalExtension();
        $fileName = $cv->getClientOriginalName();

        $cv->move($destinationPath, $fileName);

        $cert = $data['cert_file_one'];

        $destinationPath = 'uploads/certs';
        $extension = $cert->getClientOriginalExtension();
        $certName = $cert->getClientOriginalName();

        $cert->move($destinationPath, $certName);

        $nationalid = $data['id_image_one'];

        $destinationPath = 'uploads/nationalid';
        $extension = $nationalid->getClientOriginalExtension();
        $idName = $nationalid->getClientOriginalName();

        $nationalid->move($destinationPath, $idName);

        $sample1 = $data['sample_one_file'];

        $destinationPath = 'uploads/sample1';
        $extension = $sample1->getClientOriginalExtension();
        $sampleName = $sample1->getClientOriginalName();

        $sample1->move($destinationPath, $sampleName);

        $sample2 = $data['sample_two_file'];

        $destinationPath = 'uploads/sample2';
        $extension = $sample2->getClientOriginalExtension();
        $sample2Name = $sample2->getClientOriginalName();

        $sample2->move($destinationPath, $sample2Name);


     
            $user = new User;
            $user->firstname = $data['firstname'];
            $user->lastname = $data['lastname'];
            $user->username = $data['username'];
            $user->mobile_number = $data['mobile_number'];
            $user->employed = $data['employed'];
            $user->country = $data['country'];
            $user->city = $data['city'];
            $user->zip = $data['zip'];
            $user->citation = implode(',', $data['citation']);
            $user->academic_discipline = implode(',', $data['academic_discipline']);
            $user->language = $data['language'];
            $user->degree = $data['degree'];
            $user->detailed_cv = $fileName;
            $user->brief_cv = $data['brief_cv'];
            $user->cert_title = $data['cert_title'];
            $user->cert_file_one = $certName;
            $user->id_title = $data['id_title'];
            $user->id_image_one = $idName;
            $user->title_sample_one = $data['title_sample_one'];
            $user->sample_one_file = $sampleName;
            $user->title_sample_two = $data['title_sample_two'];
            $user->sample_two_file = $sample2Name;
            $user->payment_method = $data['payment_method'];
            $user->payment_details = $data['payment_details'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);

            $user->save();

            $role = Role::where('name', '=', 'writer')->firstOrFail();

            $user->roles()->attach($role);

            return $user;

    }
}


       