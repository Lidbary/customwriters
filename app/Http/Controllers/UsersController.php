<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Redirect;
use Validator;
use App\User;
use Session;
use App\Role;
use DB;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =DB::table('user_role')
        ->join('users', 'users.id', '=', 'user_role.user_id')
        ->join('roles', 'roles.id', '=', 'user_role.role_id')->get();
        return view('users.users', ['users'=>$users]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = array(
            'username'       => 'required',
            'email'      => 'required|email',
            'firstname'      => 'required',
            'lastname'      => 'required',
            'user_level'      => 'required',
            'password' => 'required|min:6|confirmed',
           
        );
        $validator = Validator::make($input_data = $request->all(), $rules);

        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
        $role_admin = Role::where('name', '=', 'admin')->firstOrFail();
        $role_editor = Role::where('name', '=', 'editor')->firstOrFail();
        $role_writer = Role::where('name', '=', 'writer')->firstOrFail();
        $newuser = New User;
        $newuser->username = $input_data['username'];
        $newuser->email = $input_data['email'];
        $newuser->firstname = $input_data['firstname'];
        $newuser->lastname = $input_data['lastname'];
        $newuser->password = bcrypt($input_data['password']);

        $newuser->save();

        if ($input_data['user_level'] === 'admin') {
            $newuser->roles()->attach($role_admin);
        } elseif ($input_data['user_level'] === 'editor') {
             $newuser->roles()->attach($role_editor);
        } else {
            $newuser->roles()->attach($role_writer);
        }

        Session::flash('success_message', 'user created successifuly!');
        return redirect()->back();

    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
