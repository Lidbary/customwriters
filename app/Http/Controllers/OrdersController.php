<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Order;
use Redirect;
use Validator;
use Auth;
use DB;
use App\User;
use App\File;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.create_order');
    }

    public function manage()
    {
        $orders = DB::table('orders')->select('orders.id AS ordersid', 'user_id', 'firstname', 'orders.citation', 'orders.created_at', 'doctype', 'order_title', 'order_level', 'max_bid', 'spacing', 'digital_sources', 'discipline', 'deadline', 'client_price', 'track_id', 'no_of_sources', 'instructions', 'no_of_pages')
        ->join('users', 'users.id', '=', 'orders.user_id')->get();
        $available = Order::where('available', 1)->get();
        return view('orders.manage-orders', ['orders'=>$orders, 'available'=>$available]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'doctype'       => 'required',
            'order_title'      => 'required',
            'order_level'      => 'required',
            'max_bid'      => 'required',
            'spacing'      => 'required',
            'digital_sources'      => 'required',
            'discipline'      => 'required',
            'deadline'      => 'required',
            'client_price'      => 'required',
            'track_id'      => 'required',
            'citation'      => 'required',
            'no_of_sources'      => 'required',
            'instructions'      => 'required',
            'no_of_pages'      => 'required'
           
        );
        $validator = Validator::make($input_data = $request->all(), $rules);

        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
  

        $neworder = New Order;
        $neworder->user_id = Auth::user()->id;
        $neworder->doctype = $input_data['doctype'];
        $neworder->order_title = $input_data['order_title'];
        $neworder->order_level = $input_data['order_level'];
        $neworder->max_bid = $input_data['max_bid'];
        $neworder->no_of_pages = $input_data['no_of_pages'];
        $neworder->spacing = $input_data['spacing'];
        $neworder->digital_sources = $input_data['digital_sources'];
        $neworder->discipline = $input_data['discipline'];
        $neworder->deadline = $input_data['deadline'];
        $neworder->client_price = $input_data['client_price'];
        $neworder->track_id = $input_data['track_id'];
        $neworder->citation = $input_data['citation'];
        $neworder->no_of_sources = $input_data['no_of_sources'];
        $neworder->instructions = explode("\n", $input_data['instructions']);

        $neworder->save();
        Session::flash('success_message', 'order created successifuly!');
        return redirect()->back();
    }
}

public function upload(Request $request) 
{
     $rules = array(
            'name'       => 'required',
           
           
        );
        $validator = Validator::make($input_data = $request->all(), $rules);

        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
  

        $newfile = New File;
        $file = $input_data['name'];

        $destinationPath = 'uploads/order_files'; // upload path
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(1111111111, 9999999999) . '.' . $extension;

        $file->move($destinationPath, $fileName); // uploading file to given path
        $newfile->name = $fileName;
        $newfile->user_id = Auth::user()->id;
        $newfile->order_id = $input_data['order_id'];

        $newfile->save();

        Session::flash('success_message', 'file uploaded successifuly!');
        return redirect()->back();
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $writers = DB::table('user_role')
            ->join('users', 'users.id', '=', 'user_role.user_id')
            ->join('roles', 'roles.id', '=', 'user_role.role_id')
            ->where('roles.name', 'writer')
            ->get();

        $files = DB::table('files')->select('files.id AS filesid', 'name')
            ->join('orders', 'orders.id', '=', 'files.order_id')
            ->join('users', 'users.id', '=', 'files.user_id')
            ->where('files.order_id', $id)
            ->get();
        $vieworder = Order::findOrFail($id);
        return view('orders.order_details', ['vieworder'=>$vieworder, 'files'=>$files, 'writers'=>$writers]);
    }


    public function assignorder(Request $request) {
        $input_data = $request->all();

        $order = Order::where('id', '=', $input_data['order_id'])->firstOrFail();
        $user = User::where('id', '=', $input_data['user_id'])->firstOrFail();

        $user->orders()->attach($order);

        Session::flash('success_message', 'Order assigned successifuly!');
        return redirect()->back();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
