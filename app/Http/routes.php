<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('manage-orders', ['uses'=> 'OrdersController@manage', 'as' => 'manage-orders']);
Route::get('new_order', ['uses'=> 'OrdersController@create', 'as' => 'new_order']);
Route::post('create_order', ['uses'=> 'OrdersController@store', 'as' => 'create_order']);
Route::post('upload_file', ['uses'=> 'OrdersController@upload', 'as' => 'upload_file']);
Route::post('assign_order', ['uses'=> 'OrdersController@assignorder', 'as' => 'assign_order']);
Route::get('create_user', ['uses'=> 'UsersController@create', 'as' => 'create_user', 'middleware'=>'roles', 'roles'=>['admin']]);
Route::post('create_user', ['uses'=> 'UsersController@store', 'as' => 'create_user']);
Route::get('{id}/view_order', ['uses'=> 'OrdersController@show', 'as' => 'view_order']);
Route::get('messages', ['uses'=> 'MessagesController@index', 'as' => 'messages']);
Route::post('new_message', ['uses'=> 'MessagesController@store', 'as' => 'new_message']);
Route::get('site_settings', ['uses'=> 'SettingsController@create', 'as' => 'site_settings']);
Route::get('payments', ['uses'=> 'PaymentsController@create', 'as' => 'payments']);
Route::get('bids', ['uses'=> 'BidsController@create', 'as' => 'bids']);
Route::get('users', ['uses'=> 'UsersController@index', 'as' => 'users']);

